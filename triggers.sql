use Shops

-- a.1
create trigger UpdateDeposit on Ownership
after update as begin
	declare @deleted_deposit decimal(9,2)
	declare @inserted_deposit decimal(9,2)

	select @deleted_deposit = deposit_amount from DELETED
	select @inserted_deposit = deposit_amount from INSERTED

	if (@deleted_deposit <> @inserted_deposit)
	begin
		declare @id_shop int
		select @id_shop = shop_id from DELETED
		
		declare @capital decimal(9,2)
		select @capital = capital from Shop where id = @id_shop

		update Shop set capital = @capital - @deleted_deposit + @inserted_deposit
		where id = @id_shop
	end
end;

select * from Ownership
select * from Shop

update Ownership set deposit_amount = 60000 where id = 1;


-- a.2
create trigger CancelingUpdateRelatedData on Ownership
after update as begin
	declare @deleted_shop_id int, @deleted_owner_id int,
		@inserted_shop_id int, @inserted_owner_id int

	select @deleted_shop_id = shop_id, @deleted_owner_id = owner_id from DELETED
	select @inserted_shop_id = shop_id, @inserted_owner_id = owner_id from INSERTED

	if (@deleted_shop_id <> @inserted_shop_id OR @deleted_owner_id <> @inserted_owner_id)
	begin
		ROLLBACK TRAN
		print '³���� ����������, � ������� ���� � ����� �������'
	end
end;

update Ownership set shop_id = 2 where id = 1;


-- b.1
alter table Ownership
    NOCHECK CONSTRAINT [FK__Ownership__shop___4316F928]

create trigger DeleteShop on Shop
after delete as begin
	declare @deleted_shop_id int
	select @deleted_shop_id = id from DELETED

	delete from Ownership where shop_id = @deleted_shop_id
end;

select * from Shop
select * from Ownership

delete from Shop where id = 10

insert into Shop(name, region, profile, capital)
values ('Test', '��������', 'test', 108000.0)

insert into Ownership(shop_id, owner_id, date, deposit_amount)
values (10, 1, '2020-01-01', 60000.0)


-- b.2
alter table Ownership
    NOCHECK CONSTRAINT [FK__Ownership__owner__440B1D61]

create trigger DeleteOwner on Owner
after delete as begin
	declare @owner_id int = (select id from DELETED);

	if (exists (select * from Ownership where owner_id = @owner_id))
	begin
		ROLLBACK TRAN
		print '³���� ����������, � ������� ���� � ����� �������(������� �� ��������)'
	end
end;

select * from Owner
select * from Ownership

delete from Owner where id = 8

insert into Owner(full_name, address, region_residence, birthday)
values ('#Test - Ivan Ivanov', null, '��������', '2000-01-01')

insert into Ownership(shop_id, owner_id, date, deposit_amount)
values (1, 8, '2010-01-01', 80500.0)


-- c.1
create trigger InsertOwner on Owner
after insert as begin
	exec TablesRows
end;

select * from Owner

insert into Owner(full_name, address, region_residence, birthday)
values ('#Test - Ivan Ivanov', null, '��������', '2000-01-01')

delete from Owner where id = 11