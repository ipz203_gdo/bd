use Shops;

create proc TablesRows as
begin
	if not exists ( select * from INFORMATION_SCHEMA.TABLES
		where TABLE_NAME = 'TableRowsCount') 
			create table TableRowsCount(
				id int identity(1, 1) not null primary key,
				table_name varchar(70),
				table_row int)
	else
		truncate table TableRowsCount

	declare cur cursor for select tabl.TABLE_NAME
		from INFORMATION_SCHEMA.TABLES tabl
		where tabl.TABLE_TYPE = 'BASE TABLE' and tabl.TABLE_NAME <> 'sysdiagrams'

	open cur
	declare @table_name varchar(70)
	fetch next from cur into @table_name

	while (@@FETCH_STATUS = 0)
	begin
		exec('insert into TableRowsCount
		values (''' + @table_name + ''', ' + '(select count(*) from ' + @table_name + '))')
	  
		fetch next from cur into @table_name
	end;

	select * from TableRowsCount

	close cur
	deallocate cur
end;

exec TablesRows;



create proc CountFields as
	select c.TABLE_NAME, count(c.COLUMN_NAME) 
	from INFORMATION_SCHEMA.COLUMNS c
		inner join INFORMATION_SCHEMA.TABLES t on c.TABLE_NAME = t.TABLE_NAME
	where t.TABLE_TYPE = 'BASE TABLE' AND c.TABLE_NAME <> 'sysdiagrams'
	group by c.TABLE_NAME;

exec CountFields



create proc TableDistinctValues
@table_name varchar(128) as
begin
	if not exists ( select * from INFORMATION_SCHEMA.TABLES
		where TABLE_NAME = 'TableFieldCountDistinctValues') 
			create table TableFieldCountDistinctValues(
				id int identity(1, 1) not null primary key,
				colum_name varchar(70),
				count_distinct_values int)
	else
		truncate table TableFieldCountDistinctValues

	declare cur cursor for select COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
		where TABLE_NAME = @table_name
	
	open cur
	declare @column_name varchar(70)
	fetch next from cur into @column_name

	while (@@FETCH_STATUS = 0)
	begin
		exec('insert into TableFieldCountDistinctValues
		values (''' + @column_name + ''', ' + '(select count(distinct ' + @column_name + ') from ' + @table_name + '))')

		fetch next from cur into @column_name
	end;

	select * from TableFieldCountDistinctValues
	
	close cur
	deallocate cur
end;

exec TableDistinctValues 'Ownership';