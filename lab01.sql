create database Shops;
use Shops;

create table Shop(
	id int identity(1, 1) not null primary key,
	name varchar(32) not null,
	region varchar(32) not null,
	profile varchar(32) not null,
	capital decimal(9,2) not null
);

create table Owner(
	id int identity(1, 1) not null primary key,
	full_name varchar(64) not null,
	address varchar(64),
	region_residence varchar(32) not null,
	birthday date not null
);

create table Ownership(
	id int identity(1, 1) not null primary key,
	shop_id int not null foreign key references Shop(id),
	owner_id int not null foreign KEY references Owner(id),
	date date not null,
	deposit_amount decimal(9,2) not null,
);



insert into Shop(name, region, profile, capital)
values ('ѳ����', '��������', '��������', 100000.0),
('����', '��������', '��������', 300000.0),
('����', '������������', '��������', 400000.0),
('��������� �����������', '������������', '�����������', 700000.0),
('��������� �����������', '��������', '�����������', 800000.0),
('ѳ����', '��������', '��������', 500000.0),
('����', '��������', '��������', 500000.0),
('ѳ����', '��������', '��������', 800000.0),
('��������� �����������', '������������', '�����������', 350000.0);

insert into Owner(full_name, address, region_residence, birthday)
values ('�������� ������ �������������', '�. ���, ���. �������, 24', '��������', '2000-01-15'),
('������� ������ ���⳿���', '�. ���, ���. ��������, 21', '��������', '2008-10-20'),
('������� ��������� ���������', '�. �������, ���. ����, 5', '������������', '2010-05-24'),
('������� ������ ��������', '�. �������, ���. ³�����, 10', '������������', '1973-02-15'),
('����������� ������ �����������', '�. �������, ���. �������, 7', '������������', '1979-03-17'),
('�������� ������ ��������', null, '��������', '1985-08-17');

insert into Ownership(shop_id, owner_id, date, deposit_amount)
values (1, 1, '2012-01-10', 60000.0),
(2, 2, '2020-05-17', 200000.0),
(3, 3, '2021-10-23', 300000.0),
(4, 4, '1995-12-05', 250000.0),
(5, 5, '1998-09-12', 125000.0),
(6, 6, '2000-05-18', 175000.0),
(7, 6, '2002-07-26', 260000.0),
(8, 6, '2010-08-22', 300000.0),
(9, 6, '2002-06-09', 80000.0);

select * from Shop
select * from Owner
select * from Ownership



create proc YoungestEntrepreneurInRegion
@region varchar(32) as begin
	declare @max_birthday date;

	select @max_birthday = max(Owner.birthday)
	from Owner
		inner join Ownership on Ownership.owner_id = Owner.id
		inner join Shop on Shop.id = Ownership.shop_id
	where Shop.region = @region;

	select Owner.full_name, Owner.birthday,
		Shop.region, count(Shop.id) as CountShops
	from Owner
		inner join Ownership on Ownership.owner_id = Owner.id
		inner join Shop on Shop.id = Ownership.shop_id
	where Owner.birthday = @max_birthday
	group by Owner.full_name, Owner.birthday, Shop.region;
end;


exec YoungestEntrepreneurInRegion '��������';


create view OwnersLess18Age as
	select Ownership.id as RegistrationNumber, Ownership.date,
		Shop.name, Shop.region,
		Owner.full_name, Owner.birthday,
		datediff(year, Owner.birthday, Ownership.date) as Age
	from Owner
		inner join Ownership on Owner.id = Ownership.shop_id
		inner join Shop on Ownership.shop_id = Shop.id
	where datediff(year, Owner.birthday, Ownership.date) < 18;

select * from OwnersLess18Age


create view MoreThan50PercentCapitalContributedOwnerLivingInAnotherRegion as
	select Ownership.id as RegistrationNumber, Owner.full_name,
		Owner.region_residence, Owner.birthday,
		Ownership.date, Ownership.deposit_amount,
		Shop.name, Shop.region, Shop.profile, Shop.capital
	from Owner
		inner join Ownership on Owner.id = Ownership.shop_id
		inner join Shop on Ownership.shop_id = Shop.id
	where Shop.region <> Owner.region_residence and
		(Ownership.deposit_amount * 100) / Shop.capital > 50;

update Ownership set deposit_amount = 425000 where id = 5;

select * from MoreThan50PercentCapitalContributedOwnerLivingInAnotherRegion


create proc ListShopsProfilesOwnedByBusinessman
@last_name varchar(32) as
	select Owner.full_name, Owner.birthday, Owner.region_residence,
		sum(Ownership.deposit_amount), Ownership.date,
		Shop.name, Shop.profile, Shop.region, Shop.capital
	from Owner
		inner join Ownership on Ownership.owner_id = Owner.id
		inner join Shop on Shop.id = Ownership.shop_id
	where Owner.full_name like '%' + @last_name + '%'
	group by Owner.full_name, Owner.birthday,
		Owner.region_residence, Ownership.date,
		Shop.name, Shop.profile, Shop.region, Shop.capital
	order by sum(Ownership.deposit_amount) desc;


exec ListShopsProfilesOwnedByBusinessman '��������';